<?php

/**
 * Created by PhpStorm.
 * User: nobrains
 * Date: 2/23/17
 * Time: 4:32 PM
 */

class PaymentResponse
{
    function __construct($rawResponse)
    {
        $this->paymentRequestID = $rawResponse["paymentRequestID"];
        $this->state = $rawResponse["state"];
        $this->merchantID = $rawResponse["merchantID"];
        $this->attributes = $rawResponse["attributes"];
        $this->receiptID = is_null($rawResponse["receiptID"]) ? null : $rawResponse["receiptID"];
        $this->time = is_null($rawResponse["time"]) ? null : $rawResponse["time"];
        $this->headers = $rawResponse["headers"];
        if (is_null($rawResponse["transactionValue"])) {
            $this->transactionValue = null;
        } else {
            $this->transactionValue = $rawResponse["transactionValue"];
        }
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return mixed
     */
    public function getMerchantID()
    {
        return $this->merchantID;
    }

    /**
     * @return mixed
     */
    public function getPaymentRequestID()
    {
        return $this->paymentRequestID;
    }

    /**
     * @return null
     */
    public function getReceiptID()
    {
        return $this->receiptID;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return null
     */
    public function getTime()
    {
        return $this->time;
    }

    public function toSignableRespresentation()
    {
        return array(
            "paymentRequestID" => $this->paymentRequestID,
            "state" => $this->state,
            "merchantID" => $this->merchantID,
            "attributes" => $this->attributes,
            "receiptID" => is_null($this->receiptID) ? null : $this->receiptID,
            "time" => is_null($this->time) ? null : $this->time,
            "transactionValue" => $this->transactionValue
        );
    }
}