<?php

/**
 * Created by PhpStorm.
 * User: nobrains
 * Date: 2/17/17
 * Time: 12:38 PM
 */

use TrafficCophp\ByteBuffer\Buffer;

class SignatureClient
{
    function __construct($public_key, $private_key, $zeta_public_key)
    {
        $this->publicKey = $public_key;
        $this->privateKey = $private_key;
        $this->zetaPublicKey = $zeta_public_key;
    }

    private function computePublicKey()
    {
        return "-----BEGIN PUBLIC KEY-----\n" . implode("\n", str_split($this->zetaPublicKey, 64)) . "\n-----END PUBLIC KEY-----";
    }

    function __destruct()
    {
        openssl_free_key($this->privateKey);
    }

    public function generate($input_array)
    {
        $flattenArray = SignatureClient::flattenArray($input_array, "");
        openssl_sign($flattenArray, $signature, $this->privateKey, OPENSSL_ALGO_SHA256);

        $PKI = self::computePKI($this->publicKey);

        $raw_IDB = 1;
        $bb = new Buffer(2 + 3 + strlen($signature));

        $bb->writeInt16BE($raw_IDB, 0);
        $bb->write($PKI, 2);
        $bb->write($signature, 5);

        return base64_encode($bb->read(0, $bb->length()));
    }

    private static function computePKI($publicKey)
    {
        $hash = hash("sha256", $publicKey, true);
        $digest = array_slice(unpack("C*", $hash), 0, 3);
        return array_reduce($digest, function ($carry, $item) {
            return $carry . chr($item);
        }, "");
    }

    static function flattenArray($input_array, $prefix)
    {
        if ($prefix !== "") {
            $prefix = "$prefix.";
        }
        ksort($input_array);
        $tmp_array = array();
        foreach ($input_array as $key => $value) {
            if (is_array($value)) {
                array_push($tmp_array, SignatureClient::flattenArray($value, "$prefix$key"));
            } else {
                array_push($tmp_array, "$prefix$key=$value");
            }
        }
        return implode("\n", $tmp_array);
    }

    public function validateResponse($paymentResponse)
    {
        $headers = $paymentResponse->getHeaders();
        if (!(isset($headers["signature"]) && isset($headers["signatoryJID"]))) {
            throw new Exception("Response is not signed");
        }
        $signature = $headers["signature"];
        $decodedSignature = base64_decode($signature);
        $bb = new Buffer($decodedSignature);
        $PKIResponseSignature = $bb->read(2, 3);

        $PKIZetaPublicKey = self::computePKI($this->zetaPublicKey);

        $validSignature = $PKIResponseSignature === $PKIZetaPublicKey;
        if (!$validSignature) {
            throw new Exception("No public key found for this signature. Please ensure you have initialized the SDK with latest public key of Zeta");
        }

        $formattedResponse = $paymentResponse->toSignableRespresentation();
        $signableRepresentation = self::flattenArray($formattedResponse, "");
        $actualSignature = $bb->read(5, $bb->length() - 5);
        $verified = openssl_verify($signableRepresentation, $actualSignature, openssl_pkey_get_public($this->computePublicKey()), OPENSSL_ALGO_SHA256);
        return $verified === 1;
    }
}
