<?php

namespace zeta;

require('SignatureClient.php');
require('model/PaymentResponse.php');
use phpish\http;

define("PWZ_URI", serialize(array(
    "requestOTP" => "/zeta.in/zetamerchant/1.0/requestUserTokenChallenge",
    "verifyOTP" => "/zeta.in/zetamerchant/1.0/verifyUserTokenChallenge",
    "hasBalance" => "/zeta.in/zetamerchant/1.0/hasSufficientBalance",
    "requestPayment" => "/zeta.in/zetamerchant/1.0/requestPayment",
    "paymentStatus" => "/zeta.in/zetamerchant/1.0/getPaymentStatus"
)));
define("SDK_VERSION", "1.0");


class pwz
{
    private $merchantID, $signatoryJID, $successURL, $failureURL, $notifyURL, $apiToken, $zetaEndPoint;
    private $signatureClient;

    function __construct($merchantID, $privateKey, $publicKey, $signatoryJID, $zetaPublicKey, $apiToken, $successURL, $failureURL, $notifyURL,$zetaEndPoint)
    {
        if (!(isset($merchantID) && isset($privateKey) && isset($publicKey) && isset($zetaPublicKey) && isset($signatoryJID))) {
            throw new \Exception('SDK not initialized properly');
        }
        $this->merchantID = $merchantID;
        $this->signatoryJID = $signatoryJID;
        $this->successURL = $successURL;
        $this->failureURL = $failureURL;
        $this->zetaEndPoint = $zetaEndPoint;
        $this->notifyURL = $notifyURL;
        $this->apiToken = $apiToken;
        $this->PWZ_URI = unserialize(PWZ_URI);
        $this->signatureClient = new \SignatureClient($publicKey, $privateKey, $zetaPublicKey);
    }

    public function requestOTPPayload($mobileNumber, $permissions)
    {
        return array(
            "phoneNumber" => getZetaMobileNumberUrl($mobileNumber),
            "merchantID" => $this->merchantID,
            "permissions" => implode(",", $permissions)
        );
    }

    public function verifyOtp($requestID, $OTP)
    {
        $verifyOTPPayload = $this->verifyOtpPayload($requestID, $OTP);
        return $this->makeAPICall($this->PWZ_URI["verifyOTP"], $verifyOTPPayload, $this->apiToken);
    }

    private function verifyOtpPayload($requestID, $OTP)
    {
        return array(
            "requestID" => $requestID,
            "otp" => $OTP,
            "merchantID" => $this->merchantID
        );
    }

    public function requestPayment($requestID, $mobileNumber, $amountInPaise, $remarks, $userToken)
    {
        $requestPaymentPayload = $this->getRequestPaymentPayload($requestID, $mobileNumber, $amountInPaise, $remarks, true, $userToken);
        return new \PaymentResponse($this->makeAPICall($this->PWZ_URI["requestPayment"], $requestPaymentPayload, $this->apiToken));
    }

    public function getPaymentStatus($paymentRequestID)
    {
        $paymentStatusPayload = $this->getPaymentStatusPayload($paymentRequestID);
        return new \PaymentResponse($this->makeAPICall($this->PWZ_URI["paymentStatus"], $paymentStatusPayload, $this->apiToken));
    }

    public function hasBalance($mobileNumber, $amountInPaise, $userToken)
    {
        $hasSufficientBalancePayload = $this->getHasSufficientBalancePayload($mobileNumber, $amountInPaise, $userToken);
        return $this->makeAPICall($this->PWZ_URI["hasBalance"], $hasSufficientBalancePayload, $this->apiToken);
    }

    private function getHasSufficientBalancePayload($mobileNumber, $amountInPaise, $userToken)
    {
        $formattedMobileNumber = getZetaMobileNumberUrl($mobileNumber);
        $request = array(
            "userIdentifier" => $formattedMobileNumber,
            "transactionValue" => array(
                "currency" => "INR",
                "amount" => $amountInPaise
            ),
            "merchantID" => $this->merchantID,
            "userToken" => $userToken,
            "time" => round(microtime(true) * 1000),
        );
        $signature = $this->signatureClient->generate($request);
        $request["headers"] = array(
            "signatoryJID" => $this->signatoryJID,
            "signature" => $signature
        );
        return $request;
    }

    public function getRequestPaymentPayload($paymentRequestID, $mobileNumber, $amountInPaise, $remarks, $autoDebit = false, $userToken = null)
    {
        $dueBy = round(microtime(true) * 1000) + (30 * 60 * 1000);
        $attributes = array(
            "pwz.remarks" => $remarks
        );
        if($autoDebit && is_null($userToken)){
            throw new \Exception("user token can't be null when auto debit is true");
        }
        if ($autoDebit) {
            $attributes["pwz.autoDebit"] = "true";
        }
        $paymentPayload = array(
            "paymentRequestID" => $paymentRequestID,
            "merchantID" => $this->merchantID,
            "dueBy" => $dueBy,
            "txnAmount" => array(
                "amount" => $amountInPaise,
                "currency" => "INR"
            ),
            "attributes" => $attributes
        );

        $signature = $this->signatureClient->generate($paymentPayload);
        $headers = array(
            "pwz.merchant.successUrl" => $this->successURL,
            "pwz.merchant.failureUrl" => $this->failureURL,
            "pwz.merchant.notifyUrl" => $this->notifyURL,
            "pwz.merchant.sdk.version" => SDK_VERSION,
            "pwz.merchant.sdk.name" => "pwz-php",
            "signatoryJID" => $this->signatoryJID,
            "signature" => $signature
        );
        if ($userToken) {
            $headers["pwz.userToken"] = $userToken;
        }
        if ($mobileNumber) {
            $headers["pwz.userIdentificationVector"] = getZetaMobileNumberUrl($mobileNumber);
        }
        $paymentPayload["headers"] = $headers;
        return $paymentPayload;
    }

    private function getPaymentStatusPayload($paymentRequestID)
    {
        $paymentStatusRequest = array(
            "paymentRequestID" => $paymentRequestID,
            "merchantID" => $this->merchantID
        );
        $signature = $this->signatureClient->generate($paymentStatusRequest);
        $paymentStatusRequest["headers"] = array(
            "signatoryJID" => $this->signatoryJID,
            "signature" => $signature
        );
        return $paymentStatusRequest;
    }

    public function validateZetaSignature($paymentResponse)
    {
        return $this->signatureClient->validateResponse($paymentResponse);
    }

    public function requestOTP($mobileNumber, $permissions)
    {
        $requestOTPPayload = $this->requestOTPPayload($mobileNumber, $permissions);
        return $this->makeAPICall($this->PWZ_URI["requestOTP"], $requestOTPPayload, $this->apiToken);
    }

    private function makeAPICall($URL, $request, $apiToken)
    {
        return http\request('POST ' . $this->zetaEndPoint . $URL, array(), $request, $response_headers, array("X-Zeta-Authtoken" => $apiToken, "content-type" => "application/json; charset=UTF-8", "Cache-Control" => "no-cache"));
    }
}

function getZetaMobileNumberUrl($mobileNumber)
{
    if (strlen($mobileNumber) === 10) {
        return "p:+91" . $mobileNumber;
    }
    if (substr($mobileNumber, 0, 3) === "+91" && strlen($mobileNumber) == 13) {
        return "p:" . $mobileNumber;
    }
    if (substr($mobileNumber, 0, 3) === "p:+91" && strlen($mobileNumber) == 15) {
        return $mobileNumber;
    }
    throw new \Exception("Invalid mobile number format. Correct format +9199XXXXXXXXXX or 99XXXXXXXXXX");
}


?>